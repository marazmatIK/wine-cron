package com.nchernov.mock.mockdata;

/**
 * Created by nchernov on 06.10.2015.
 */
public class MockData {
    public static final String XML = "<Catalog xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "    <Status>\n" +
            "        <Messages xmlns:a=\"http://schemas.microsoft.com/2003/10/Serialization/Arrays\"/>\n" +
            "        <ReturnCode>0</ReturnCode>\n" +
            "    </Status>\n" +
            "    <Products>\n" +
            "        <List>\n" +
            "            <Product>\n" +
            "                <Id>147732</Id>\n" +
            "                <Name>Fattoria dei Barbi Brunello di Montalcino 2010 3-pack</Name>\n" +
            "                <Url>http://www.wine.com/v6/Fattoria-dei-Barbi-Brunello-di-Montalcino-2010-3-pack/gift/147732/Detail.aspx</Url>\n" +
            "                <Appellation i:nil=\"true\"/>\n" +
            "                <Labels>\n" +
            "                    <Label>\n" +
            "                        <Id>147732m</Id>\n" +
            "                        <Name>thumbnail</Name>\n" +
            "                        <Url>http://cache.wine.com/labels/147732m.jpg</Url>\n" +
            "                    </Label>\n" +
            "                </Labels>\n" +
            "                <Type>WineSet</Type>\n" +
            "                <Varietal i:nil=\"true\"/>\n" +
            "                <Vineyard i:nil=\"true\"/>\n" +
            "                <Vintage/>\n" +
            "                <Community>\n" +
            "                    <Reviews>\n" +
            "                        <HighestScore>0</HighestScore>\n" +
            "                        <List/>\n" +
            "                        <Url>http://www.wine.com/v6/Fattoria-dei-Barbi-Brunello-di-Montalcino-2010-3-pack/gift/147732/Detail.aspx?pageType=reviews</Url>\n" +
            "                    </Reviews>\n" +
            "                    <Url>http://www.wine.com/v6/Fattoria-dei-Barbi-Brunello-di-Montalcino-2010-3-pack/gift/147732/Detail.aspx</Url>\n" +
            "                </Community>\n" +
            "                <Description>Intense, vivid ruby red color with aromas of red berries, maraschino cherry and liquorice. The aromas repeat on the palate with more hints of liquorice and tobacco. Tannins are firm and a fruit is refreshed by a lively acidity that leads to a long, persistent finish</Description>\n" +
            "                <GeoLocation>\n" +
            "                    <Latitude>-360</Latitude>\n" +
            "                    <Longitude>-360</Longitude>\n" +
            "                    <Url>http://www.wine.com/v6/aboutwine/mapof.aspx?winery=2124</Url>\n" +
            "                </GeoLocation>\n" +
            "                <PriceMax>120.0000</PriceMax>\n" +
            "                <PriceMin>120.0000</PriceMin>\n" +
            "                <PriceRetail>165.0000</PriceRetail>\n" +
            "                <ProductAttributes/>\n" +
            "                <Ratings>\n" +
            "                    <HighestScore>96</HighestScore>\n" +
            "                    <List/>\n" +
            "                </Ratings>\n" +
            "                <Retail i:nil=\"true\"/>\n" +
            "                <Vintages>\n" +
            "                    <List/>\n" +
            "                </Vintages>\n" +
            "            </Product>\n" +
            "            <Product>\n" +
            "                <Id>143195</Id>\n" +
            "                <Name>Fattoria Le Pupille Elisabetta Geppetti 'Saffredi' 2012</Name>\n" +
            "                <Url>http://www.wine.com/v6/Fattoria-Le-Pupille-Elisabetta-Geppetti-Saffredi-2012/wine/143195/Detail.aspx</Url>\n" +
            "                <Appellation>\n" +
            "                    <Id>2452</Id>\n" +
            "                    <Name>Tuscany</Name>\n" +
            "                    <Url>http://www.wine.com/v6/Tuscany/wine/list.aspx?N=7155+105+2452</Url>\n" +
            "                    <Region>\n" +
            "                        <Id>105</Id>\n" +
            "                        <Name>Italy</Name>\n" +
            "                        <Url>http://www.wine.com/v6/Italy/wine/list.aspx?N=7155+105</Url>\n" +
            "                        <Area i:nil=\"true\"/>\n" +
            "                    </Region>\n" +
            "                </Appellation>\n" +
            "                <Labels>\n" +
            "                    <Label>\n" +
            "                        <Id>143195m</Id>\n" +
            "                        <Name>thumbnail</Name>\n" +
            "                        <Url>http://cache.wine.com/labels/143195m.jpg</Url>\n" +
            "                    </Label>\n" +
            "                </Labels>\n" +
            "                <Type>Wine</Type>\n" +
            "                <Varietal>\n" +
            "                    <Id>145</Id>\n" +
            "                    <Name>Other Red Blends</Name>\n" +
            "                    <Url>http://www.wine.com/v6/Other-Red-Blends/wine/list.aspx?N=7155+124+145</Url>\n" +
            "                    <WineType>\n" +
            "                        <Id>124</Id>\n" +
            "                        <Name>Red Wines</Name>\n" +
            "                        <Url>http://www.wine.com/v6/Red-Wines/wine/list.aspx?N=7155+124</Url>\n" +
            "                    </WineType>\n" +
            "                </Varietal>\n" +
            "                <Vineyard>\n" +
            "                    <Id>999997461</Id>\n" +
            "                    <Name>Fattoria Le Pupille</Name>\n" +
            "                    <Url>http://www.wine.com/v6/Fattoria-Le-Pupille/learnabout.aspx?winery=20057</Url>\n" +
            "                    <ImageUrl>http://cache.wine.com/aboutwine/basics/images/winerypics/20057.jpg</ImageUrl>\n" +
            "                    <GeoLocation>\n" +
            "                        <Latitude>-360</Latitude>\n" +
            "                        <Longitude>-360</Longitude>\n" +
            "                        <Url>http://www.wine.com/v6/aboutwine/mapof.aspx?winery=20057</Url>\n" +
            "                    </GeoLocation>\n" +
            "                </Vineyard>\n" +
            "                <Vintage/>\n" +
            "                <Community>\n" +
            "                    <Reviews>\n" +
            "                        <HighestScore>0</HighestScore>\n" +
            "                        <List/>\n" +
            "                        <Url>http://www.wine.com/v6/Fattoria-Le-Pupille-Elisabetta-Geppetti-Saffredi-2012/wine/143195/Detail.aspx?pageType=reviews</Url>\n" +
            "                    </Reviews>\n" +
            "                    <Url>http://www.wine.com/v6/Fattoria-Le-Pupille-Elisabetta-Geppetti-Saffredi-2012/wine/143195/Detail.aspx</Url>\n" +
            "                </Community>\n" +
            "                <Description/>\n" +
            "                <GeoLocation>\n" +
            "                    <Latitude>-360</Latitude>\n" +
            "                    <Longitude>-360</Longitude>\n" +
            "                    <Url>http://www.wine.com/v6/aboutwine/mapof.aspx?winery=20057</Url>\n" +
            "                </GeoLocation>\n" +
            "                <PriceMax>99.0000</PriceMax>\n" +
            "                <PriceMin>99.0000</PriceMin>\n" +
            "                <PriceRetail>99.0000</PriceRetail>\n" +
            "                <ProductAttributes>\n" +
            "                    <ProductAttribute>\n" +
            "                        <Id>36</Id>\n" +
            "                        <Name>Collectible Wines</Name>\n" +
            "                        <Url>http://www.wine.com/v6/Collectible-Wines/wine/list.aspx?N=7155+36</Url>\n" +
            "                        <ImageUrl>http://cache.wine.com/assets/glo_icon_collectable_big.gif</ImageUrl>\n" +
            "                    </ProductAttribute>\n" +
            "                </ProductAttributes>\n" +
            "                <Ratings>\n" +
            "                    <HighestScore>100</HighestScore>\n" +
            "                    <List/>\n" +
            "                </Ratings>\n" +
            "                <Retail i:nil=\"true\"/>\n" +
            "                <Vintages>\n" +
            "                    <List/>\n" +
            "                </Vintages>\n" +
            "            </Product>\n" +
            "            <Product>\n" +
            "                <Id>142069</Id>\n" +
            "                <Name>Fattoria dei Barbi Brunello di Montalcino 2010</Name>\n" +
            "                <Url>http://www.wine.com/v6/Fattoria-dei-Barbi-Brunello-di-Montalcino-2010/wine/142069/Detail.aspx</Url>\n" +
            "                <Appellation>\n" +
            "                    <Id>2452</Id>\n" +
            "                    <Name>Tuscany</Name>\n" +
            "                    <Url>http://www.wine.com/v6/Tuscany/wine/list.aspx?N=7155+105+2452</Url>\n" +
            "                    <Region>\n" +
            "                        <Id>105</Id>\n" +
            "                        <Name>Italy</Name>\n" +
            "                        <Url>http://www.wine.com/v6/Italy/wine/list.aspx?N=7155+105</Url>\n" +
            "                        <Area i:nil=\"true\"/>\n" +
            "                    </Region>\n" +
            "                </Appellation>\n" +
            "                <Labels>\n" +
            "                    <Label>\n" +
            "                        <Id>142069m</Id>\n" +
            "                        <Name>thumbnail</Name>\n" +
            "                        <Url>http://cache.wine.com/labels/142069m.jpg</Url>\n" +
            "                    </Label>\n" +
            "                </Labels>\n" +
            "                <Type>Wine</Type>\n" +
            "                <Varietal>\n" +
            "                    <Id>163</Id>\n" +
            "                    <Name>Sangiovese</Name>\n" +
            "                    <Url>http://www.wine.com/v6/Sangiovese/wine/list.aspx?N=7155+124+163</Url>\n" +
            "                    <WineType>\n" +
            "                        <Id>124</Id>\n" +
            "                        <Name>Red Wines</Name>\n" +
            "                        <Url>http://www.wine.com/v6/Red-Wines/wine/list.aspx?N=7155+124</Url>\n" +
            "                    </WineType>\n" +
            "                </Varietal>\n" +
            "                <Vineyard>\n" +
            "                    <Id>7746</Id>\n" +
            "                    <Name>Fattoria dei Barbi</Name>\n" +
            "                    <Url>http://www.wine.com/v6/Fattoria-dei-Barbi/learnabout.aspx?winery=2124</Url>\n" +
            "                    <ImageUrl>http://cache.wine.com/aboutwine/basics/images/winerypics/2124.jpg</ImageUrl>\n" +
            "                    <GeoLocation>\n" +
            "                        <Latitude>-360</Latitude>\n" +
            "                        <Longitude>-360</Longitude>\n" +
            "                        <Url>http://www.wine.com/v6/aboutwine/mapof.aspx?winery=2124</Url>\n" +
            "                    </GeoLocation>\n" +
            "                </Vineyard>\n" +
            "                <Vintage/>\n" +
            "                <Community>\n" +
            "                    <Reviews>\n" +
            "                        <HighestScore>0</HighestScore>\n" +
            "                        <List/>\n" +
            "                        <Url>http://www.wine.com/v6/Fattoria-dei-Barbi-Brunello-di-Montalcino-2010/wine/142069/Detail.aspx?pageType=reviews</Url>\n" +
            "                    </Reviews>\n" +
            "                    <Url>http://www.wine.com/v6/Fattoria-dei-Barbi-Brunello-di-Montalcino-2010/wine/142069/Detail.aspx</Url>\n" +
            "                </Community>\n" +
            "                <Description/>\n" +
            "                <GeoLocation>\n" +
            "                    <Latitude>-360</Latitude>\n" +
            "                    <Longitude>-360</Longitude>\n" +
            "                    <Url>http://www.wine.com/v6/aboutwine/mapof.aspx?winery=2124</Url>\n" +
            "                </GeoLocation>\n" +
            "                <PriceMax>44.9900</PriceMax>\n" +
            "                <PriceMin>44.9900</PriceMin>\n" +
            "                <PriceRetail>55.0000</PriceRetail>\n" +
            "                <ProductAttributes>\n" +
            "                    <ProductAttribute>\n" +
            "                        <Id>613</Id>\n" +
            "                        <Name>Big &amp;amp; Bold</Name>\n" +
            "                        <Url>http://www.wine.com/v6/Big-andamp-Bold/wine/list.aspx?N=7155+613</Url>\n" +
            "                        <ImageUrl/>\n" +
            "                    </ProductAttribute>\n" +
            "                    <ProductAttribute>\n" +
            "                        <Id>36</Id>\n" +
            "                        <Name>Collectible Wines</Name>\n" +
            "                        <Url>http://www.wine.com/v6/Collectible-Wines/wine/list.aspx?N=7155+36</Url>\n" +
            "                        <ImageUrl>http://cache.wine.com/assets/glo_icon_collectable_big.gif</ImageUrl>\n" +
            "                    </ProductAttribute>\n" +
            "                </ProductAttributes>\n" +
            "                <Ratings>\n" +
            "                    <HighestScore>96</HighestScore>\n" +
            "                    <List/>\n" +
            "                </Ratings>\n" +
            "                <Retail i:nil=\"true\"/>\n" +
            "                <Vintages>\n" +
            "                    <List/>\n" +
            "                </Vintages>\n" +
            "            </Product>\n" +
            "            <Product>\n" +
            "                <Id>130091</Id>\n" +
            "                <Name>Faustino I Gran Reserva 2001</Name>\n" +
            "                <Url>http://www.wine.com/v6/Faustino-I-Gran-Reserva-2001/wine/130091/Detail.aspx</Url>\n" +
            "                <Appellation>\n" +
            "                    <Id>2370</Id>\n" +
            "                    <Name>Rioja</Name>\n" +
            "                    <Url>http://www.wine.com/v6/Rioja/wine/list.aspx?N=7155+109+2370</Url>\n" +
            "                    <Region>\n" +
            "                        <Id>109</Id>\n" +
            "                        <Name>Spain</Name>\n" +
            "                        <Url>http://www.wine.com/v6/Spain/wine/list.aspx?N=7155+109</Url>\n" +
            "                        <Area i:nil=\"true\"/>\n" +
            "                    </Region>\n" +
            "                </Appellation>\n" +
            "                <Labels>\n" +
            "                    <Label>\n" +
            "                        <Id>130091m</Id>\n" +
            "                        <Name>thumbnail</Name>\n" +
            "                        <Url>http://cache.wine.com/labels/130091m.jpg</Url>\n" +
            "                    </Label>\n" +
            "                </Labels>\n" +
            "                <Type>Wine</Type>\n" +
            "                <Varietal>\n" +
            "                    <Id>169</Id>\n" +
            "                    <Name>Tempranillo</Name>\n" +
            "                    <Url>http://www.wine.com/v6/Tempranillo/wine/list.aspx?N=7155+124+169</Url>\n" +
            "                    <WineType>\n" +
            "                        <Id>124</Id>\n" +
            "                        <Name>Red Wines</Name>\n" +
            "                        <Url>http://www.wine.com/v6/Red-Wines/wine/list.aspx?N=7155+124</Url>\n" +
            "                    </WineType>\n" +
            "                </Varietal>\n" +
            "                <Vineyard>\n" +
            "                    <Id>8777</Id>\n" +
            "                    <Name>Faustino</Name>\n" +
            "                    <Url>http://www.wine.com/v6/Faustino/learnabout.aspx?winery=1977</Url>\n" +
            "                    <ImageUrl>http://cache.wine.com/aboutwine/basics/images/winerypics/1977.jpg</ImageUrl>\n" +
            "                    <GeoLocation>\n" +
            "                        <Latitude>-360</Latitude>\n" +
            "                        <Longitude>-360</Longitude>\n" +
            "                        <Url>http://www.wine.com/v6/aboutwine/mapof.aspx?winery=1977</Url>\n" +
            "                    </GeoLocation>\n" +
            "                </Vineyard>\n" +
            "                <Vintage/>\n" +
            "                <Community>\n" +
            "                    <Reviews>\n" +
            "                        <HighestScore>5</HighestScore>\n" +
            "                        <List/>\n" +
            "                        <Url>http://www.wine.com/v6/Faustino-I-Gran-Reserva-2001/wine/130091/Detail.aspx?pageType=reviews</Url>\n" +
            "                    </Reviews>\n" +
            "                    <Url>http://www.wine.com/v6/Faustino-I-Gran-Reserva-2001/wine/130091/Detail.aspx</Url>\n" +
            "                </Community>\n" +
            "                <Description/>\n" +
            "                <GeoLocation>\n" +
            "                    <Latitude>-360</Latitude>\n" +
            "                    <Longitude>-360</Longitude>\n" +
            "                    <Url>http://www.wine.com/v6/aboutwine/mapof.aspx?winery=1977</Url>\n" +
            "                </GeoLocation>\n" +
            "                <PriceMax>39.9900</PriceMax>\n" +
            "                <PriceMin>29.9900</PriceMin>\n" +
            "                <PriceRetail>38.0000</PriceRetail>\n" +
            "                <ProductAttributes>\n" +
            "                    <ProductAttribute>\n" +
            "                        <Id>611</Id>\n" +
            "                        <Name>Smooth &amp;amp; Supple</Name>\n" +
            "                        <Url>http://www.wine.com/v6/Smooth-andamp-Supple/wine/list.aspx?N=7155+611</Url>\n" +
            "                        <ImageUrl/>\n" +
            "                    </ProductAttribute>\n" +
            "                    <ProductAttribute>\n" +
            "                        <Id>0</Id>\n" +
            "                        <Name>Older Vintages</Name>\n" +
            "                        <Url/>\n" +
            "                        <ImageUrl/>\n" +
            "                    </ProductAttribute>\n" +
            "                </ProductAttributes>\n" +
            "                <Ratings>\n" +
            "                    <HighestScore>98</HighestScore>\n" +
            "                    <List/>\n" +
            "                </Ratings>\n" +
            "                <Retail i:nil=\"true\"/>\n" +
            "                <Vintages>\n" +
            "                    <List/>\n" +
            "                </Vintages>\n" +
            "            </Product>\n" +
            "            <Product>\n" +
            "                <Id>145981</Id>\n" +
            "                <Name>Atlas Peak Napa Valley Mountain Series Cabernet 3-Pack</Name>\n" +
            "                <Url>http://www.wine.com/v6/Atlas-Peak-Napa-Valley-Mountain-Series-Cabernet-3-Pack/gift/145981/Detail.aspx</Url>\n" +
            "                <Appellation i:nil=\"true\"/>\n" +
            "                <Labels>\n" +
            "                    <Label>\n" +
            "                        <Id>145981m</Id>\n" +
            "                        <Name>thumbnail</Name>\n" +
            "                        <Url>http://cache.wine.com/labels/145981m.jpg</Url>\n" +
            "                    </Label>\n" +
            "                </Labels>\n" +
            "                <Type>WineSet</Type>\n" +
            "                <Varietal i:nil=\"true\"/>\n" +
            "                <Vineyard i:nil=\"true\"/>\n" +
            "                <Vintage/>\n" +
            "                <Community>\n" +
            "                    <Reviews>\n" +
            "                        <HighestScore>0</HighestScore>\n" +
            "                        <List/>\n" +
            "                        <Url>http://www.wine.com/v6/Atlas-Peak-Napa-Valley-Mountain-Series-Cabernet-3-Pack/gift/145981/Detail.aspx?pageType=reviews</Url>\n" +
            "                    </Reviews>\n" +
            "                    <Url>http://www.wine.com/v6/Atlas-Peak-Napa-Valley-Mountain-Series-Cabernet-3-Pack/gift/145981/Detail.aspx</Url>\n" +
            "                </Community>\n" +
            "                <Description>&lt;b&gt;Each Atlas Peak Mountain Series 3-Pack includes:&lt;/b&gt;&lt;p&gt;&lt;b&gt;Atlas Peak Mountain Cabernet Sauvignon 2007:&lt;/b&gt;Virtually all of the Atlas Peak’s vineyards receive full sunlight throughout the day, with cooling afternoon winds from the San Francisco Bay.  Atlas Peak’s soils are well-drained and derive from volcanic material. Deep brick red color. Notes of mocha, espresso, Chai, and blackberry on the nose. Plush and generous with dark berry fruit and coffee-laced notes woven into the wine. Savory, supple and soft in the mouth with lingering but yielding tannins.&lt;p&gt;&lt;b&gt;Atlas Peak Mount Veeder Cabernet Sauvignon 2007:&lt;/b&gt;&lt;p&gt;\"An amazing effort, the ultra-fine 2007 Atlas Peak Mount Veeder Cabernet is a handsome wine that is drinking pretty fine now. Deep ruby, blackish color; bold black fruit, enticing anise and chalk in the nose; full bodied, lavish and packed on the palate; super long finish, fully loaded with some orange peel in the aftertaste. \"-&lt;b&gt;93 points, Wilfred Wong&lt;/b&gt;&lt;p&gt;&lt;b&gt;Atlas Peak Spring Mountain Cabernet Sauvignon 2007:&lt;/b&gt;&lt;p&gt;\"There is a lot to like about the 2007 vintage and the Atlas Peak Spring Mountain displays superb balance of elements. From fruit to leaves to earth, this one has it all. Medium to dark ruby color; frisky red and black fruit in the nose; full bodied, tannic; leathery flavors, black fruit and dust; medium to long finish.\"- &lt;b&gt;91 points, Wilfred Wong&lt;/b&gt;</Description>\n" +
            "                <GeoLocation>\n" +
            "                    <Latitude>-360</Latitude>\n" +
            "                    <Longitude>-360</Longitude>\n" +
            "                    <Url>http://www.wine.com/v6/aboutwine/mapof.aspx?winery=82</Url>\n" +
            "                </GeoLocation>\n" +
            "                <PriceMax>89.9900</PriceMax>\n" +
            "                <PriceMin>89.9900</PriceMin>\n" +
            "                <PriceRetail>195.0000</PriceRetail>\n" +
            "                <ProductAttributes/>\n" +
            "                <Ratings>\n" +
            "                    <HighestScore>0</HighestScore>\n" +
            "                    <List/>\n" +
            "                </Ratings>\n" +
            "                <Retail i:nil=\"true\"/>\n" +
            "                <Vintages>\n" +
            "                    <List/>\n" +
            "                </Vintages>\n" +
            "            </Product>\n" +
            "        </List>\n" +
            "        <Offset>0</Offset>\n" +
            "        <Total>6</Total>\n" +
            "        <Url/>\n" +
            "    </Products>\n" +
            "</Catalog>";
}
