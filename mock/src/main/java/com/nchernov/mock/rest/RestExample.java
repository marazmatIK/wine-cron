package com.nchernov.mock.rest;

import com.nchernov.mock.mockdata.MockData;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by zugzug on 19.09.15.
 */
@Path("catalog")
public class RestExample {

    @GET
    public Response sayHello(@QueryParam("offset") Long offset, @QueryParam("size") Integer size) throws Exception {
        return Response.status(Response.Status.OK)
                .entity(MockData.XML)
                .type(MediaType.APPLICATION_XML)
                .build();
    }

}
