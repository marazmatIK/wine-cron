package com.nchernov.winecron.client.impl;

import com.nchernov.winecron.client.WineClient;
import com.nchernov.winecron.client.impl.WineClientImpl;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

/**
 * Created by nchernov on 06.10.2015.
 */
public class WineClientTest {
    WineClient wineClient = spy(new WineClientImpl("xxx", "localhost"));

    @Test(expected = IllegalArgumentException.class)
    public void incorrectParameter() {
        wineClient.loadItems(5, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectBothParameters() {
        wineClient.loadItems(0, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectNegativeBothParameters() {
        wineClient.loadItems(-1, -2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectNegativeParameter() {
        wineClient.loadItems(-1, 2);
    }

    @Test(expected = IllegalStateException.class)
    public void happyDayScenario() {
        wineClient.loadItems(4, 2);
    }
}
