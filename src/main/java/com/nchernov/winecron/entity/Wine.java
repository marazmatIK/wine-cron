package com.nchernov.winecron.entity;

/**
 * Created by nchernov on 06.10.2015.
 */
public abstract class Wine {

    public abstract String getTitle();

    public abstract void setTitle(String title);

    public abstract int getYear();

    public abstract void setYear(int year);

    public abstract float getPriceMax();

    public abstract void setPriceMax(float priceMax);

    public abstract float getPriceMin();

    public abstract void setPriceMin(float priceMin);

    public abstract float getPriceRetail();

    public abstract void setPriceRetail(float priceRetail);

    public abstract String getProducer();

    public abstract void setProducer(String producer);

    public abstract Long getParsedId();

    public abstract void setParsedId(Long parsedId);

    /*public abstract String getDescription();

    public abstract void setDescription(String description);*/

    public abstract String getUrl();

    public abstract void setUrl(String url);

    @Override
    public int hashCode() {
        int seed = 3;
        int additionalSeed = 5;
        int hash = (getTitle() == null) ? 1 * seed : getTitle().hashCode() * seed;
        return hash + getYear() * additionalSeed;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Wine)) {
            return false;
        }
        final Wine other = (Wine) obj;
        if ((this.getTitle() == null) ? (other.getTitle() != null) : !(this.getYear() == other.getYear())) {
            return false;
        }
        return true;
    }

    public String toString() {
        return String.format("%s; %s; %s", getTitle(), getPriceMax(), getProducer());
    }
}
