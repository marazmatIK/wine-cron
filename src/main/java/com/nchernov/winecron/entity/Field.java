package com.nchernov.winecron.entity;

/**
 * Created by nchernov on 06.10.2015.
 */
public enum Field {
    PRICE, TITLE, DESCRIPTION, PRODUCER;
}
