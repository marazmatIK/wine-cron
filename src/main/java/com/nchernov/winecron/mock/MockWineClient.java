package com.nchernov.winecron.mock;

import com.nchernov.winecron.client.WineClient;
import com.nchernov.winecron.client.impl.jaxb.Catalog;
import com.nchernov.winecron.client.impl.jaxb.TestData;
import com.nchernov.winecron.client.impl.jaxb.context.ContextProvider;
import com.nchernov.winecron.entity.Field;
import com.nchernov.winecron.entity.Wine;
import com.nchernov.winecron.storage.StorableWine;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

/**
 * Created by nchernov on 06.10.2015.
 */
public class MockWineClient implements WineClient {
    private Catalog catalog;
    private static final Logger log = Logger.getLogger(MockWineClient.class.getName());

    Catalog unmarshall(String string) {
        try {
            JAXBContext jaxbContext = ContextProvider.getInstance().getJaxbContext();
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (Catalog) jaxbUnmarshaller.unmarshal(new StringReader(string));
        } catch (JAXBException ex) {
            //log.error("Cannot build Config object from properties!", ex);
            return new Catalog();
        }
    }

    Catalog unmarshall(Reader reader) {
        try {
            JAXBContext jaxbContext = ContextProvider.getInstance().getJaxbContext();
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (Catalog) jaxbUnmarshaller.unmarshal(reader);
        } catch (JAXBException ex) {
            //log.error("Cannot build Config object from properties!", ex);
            return new Catalog();
        }
    }

    public MockWineClient() {
        catalog = unmarshall(TestData.XML);
    }


    @Override
    public List<? extends Wine> loadItems(long startFrom, int limit) {
        log.info(String.format("Issuing request for wines for range <%s; %s>",
                new Long(startFrom).toString(), new Long(startFrom + limit).toString()));
        List<StorableWine> wineCatalogItems = catalog.getProducts().getProducts();
        return wineCatalogItems.subList((int)startFrom, (int)startFrom + limit);
    }

    @Override
    public List<? extends Wine> updateItems(Field field) {
        return null;
    }
}
