package com.nchernov.winecron.client.impl.jaxb.context;

import com.nchernov.winecron.client.impl.jaxb.Catalog;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Created by nchernov on 06.10.2015.
 */
public final class ContextProvider {
    private static JAXBContext jaxbContext;
    private static ContextProvider instance;
    private ContextProvider() throws JAXBException {
        jaxbContext = JAXBContext.newInstance(Catalog.class);
    }

    public static final synchronized ContextProvider getInstance() throws JAXBException {
        if (instance == null) {
            instance = new ContextProvider();
        }
        return instance;
    }

    public final JAXBContext getJaxbContext() {
        return jaxbContext;
    }
}
