package com.nchernov.winecron.client.impl.jaxb;

import com.nchernov.winecron.storage.StorableWine;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nchernov on 06.10.2015.
 */
@XmlRootElement(name = "Products")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Products {
    private List<StorableWine> list = new ArrayList<StorableWine>();
    private int offset;
    private int total;

    @XmlElement(name = "Product")
    @XmlElementWrapper(name = "List")
    public List<StorableWine> getProducts() {
        return list;
    }

    public void setProducts(List<StorableWine> list) {
        this.list = list;
    }

    @XmlElement(name = "Offset")
    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    @XmlElement(name = "Total")
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
