package com.nchernov.winecron.client.impl.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by nchernov on 06.10.2015.
 */
@XmlRootElement(name = "Catalog")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Catalog {
    private Status status = new Status();
    private Products products = new Products();

    @XmlElement(name = "Status")
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @XmlElement(name = "Products")
    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }
}
