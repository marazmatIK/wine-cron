package com.nchernov.winecron.client.impl;

import com.nchernov.winecron.client.WineClient;
import com.nchernov.winecron.client.impl.jaxb.Catalog;
import com.nchernov.winecron.client.impl.jaxb.context.ContextProvider;
import com.nchernov.winecron.entity.Field;
import com.nchernov.winecron.entity.Wine;
import com.nchernov.winecron.storage.StorableWine;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

/**
 * Implementation for Wine API of wine.com archive
 */
public class WineClientImpl implements WineClient {
    private final DefaultHttpClient httpClient;
    private final String endPoint;
    private String apiKey;
    private static final String END_POINT = "http://services.wine.com/api/beta/service.svc/xml/catalog";
    private static final String END_POINT_DEFAULT = "http://services.wine.com/api/beta/service.svc/xml/catalog";
    private static final String END_POINT_QUERY_STRING = "apikey=%s&size=%s&offset=%s";
    private static final Logger log = Logger.getLogger(WineClientImpl.class.getName());

    public WineClientImpl(String apiKey, String endPoint) {
        this.apiKey = apiKey;
        this.endPoint = StringUtils.isEmpty(endPoint) ? END_POINT_DEFAULT : endPoint;
        log.info("Using end point for requests: " + endPoint);
        httpClient = new DefaultHttpClient();
    }

    @Override
    public List<? extends Wine> loadItems(long startFrom, int limit) {
        if (limit <= 0) {
            throw new IllegalArgumentException("Limit (size) can be only positive");
        }
        if (startFrom <0 ) {
            throw new IllegalArgumentException("Offset cannot be negative");
        }
        log.info(String.format("Issuing request for wines for range <%s; %s>",
                new Long(startFrom).toString(), new Long(startFrom + limit).toString()));
        try {
            return requestWineItems(startFrom, limit);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Wine> updateItems(Field field) {
        return null;
    }

    Catalog unmarshall(String string) {
        try {
            JAXBContext jaxbContext = ContextProvider.getInstance().getJaxbContext();
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (Catalog) jaxbUnmarshaller.unmarshal(new StringReader(string));
        } catch (JAXBException ex) {
            //log.error("Cannot build Config object from properties!", ex);
            return new Catalog();
        }
    }

    Catalog unmarshall(Reader reader) {
        try {
            JAXBContext jaxbContext = ContextProvider.getInstance().getJaxbContext();
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (Catalog) jaxbUnmarshaller.unmarshal(reader);
        } catch (JAXBException ex) {
            //log.error("Cannot build Config object from properties!", ex);
            return new Catalog();
        }
    }

    List<StorableWine> requestWineItems(long offset, int size) throws IOException {
        HttpGet getRequest = new HttpGet(String.format(endPoint + "?" + END_POINT_QUERY_STRING, apiKey, new Long(size).toString(), new Long(offset).toString()));
        getRequest.addHeader("accept", "application/json");

        HttpResponse response = httpClient.execute(getRequest);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatusLine().getStatusCode());
        }
        InputStreamReader reader = new InputStreamReader((response.getEntity().getContent()));
        Catalog catalog = unmarshall(reader);
        reader.close();
        return catalog.getProducts().getProducts();
    }
}
