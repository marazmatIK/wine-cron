package com.nchernov.winecron.client.impl.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by nchernov on 06.10.2015.
 */
@XmlRootElement(name = "Status")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Status {
    private String messages;
    private int returnCode;

    @XmlElement(name = "Messages")
    public String getMessages() {
        return messages;
    }

    public void setMessage(String messages) {
        this.messages = messages;
    }

    @XmlElement(name = "ReturnCode")
    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }
}
