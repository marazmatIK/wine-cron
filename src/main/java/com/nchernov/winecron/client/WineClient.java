package com.nchernov.winecron.client;

import com.nchernov.winecron.entity.Field;
import com.nchernov.winecron.entity.Wine;

import java.util.List;

/**
 * Created by nchernov on 06.10.2015.
 */
public interface WineClient {
    public List<? extends Wine> loadItems(long startFrom, int limit);
    public List<? extends Wine> updateItems(Field field);

}
