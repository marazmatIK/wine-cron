package com.nchernov.winecron.config;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.Writer;
import java.util.Observable;

import org.apache.log4j.Logger;

/**
 * Created by nchernov on 06.10.2015.
 */
public class ConfigService extends Observable {
    private static final Logger log = Logger.getLogger(ConfigService.class.getName());

    public ConfigService() {
    }

    public Configuration getConfig(String filePath) throws IOException {
        String configStr = readFile(filePath);
        if (configStr == null || configStr.isEmpty()) {
            return new Configuration();
        }
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Configuration.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (Configuration) jaxbUnmarshaller.unmarshal(new StringReader(configStr));
        } catch (JAXBException ex) {
            log.error(String.format("Cannot build Config object from file %s!", filePath), ex);
            return new Configuration();
        }
    }

    public void setConfig(Configuration config, String filePath) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Configuration.class);
            Marshaller jaxbUnmarshaller = jaxbContext.createMarshaller();
            Writer writer = new FileWriter(new File(filePath));
            jaxbUnmarshaller.marshal(config, writer);
            FileWriter fileWriter = new FileWriter(new File(filePath));
            writer.close();
            setChanged();
            notifyObservers(config);
        } catch (JAXBException ex) {
            log.error("Cannot serialize Config object!", ex);
        } catch (IOException e) {
            log.error("Cannot serialize Config object!", e);
        }
    }

    public void marshallObject(Object object, OutputStream output) throws IOException {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            jaxbMarshaller.marshal(object, output);
        } catch (JAXBException ex) {
            throw new IOException(ex);
        }
    }

    public String marshallObject(Object object) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        marshallObject(object, byteArrayOutputStream);
        return new String(byteArrayOutputStream.toByteArray(), "UTF-8");
    }

    public <T> T unmarshallObject(Class<T> type, String string) throws Exception {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(type);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            return (T) unmarshaller.unmarshal(new StringReader(string));
        } catch (JAXBException ex) {
            log.error("Cannot build Config object from properties!", ex);
            throw new IOException(ex);
        }
    }

    String readFile(String filename) throws IOException {
        String content = null;
        File file = new File(filename); //for ex foo.txt
        FileReader reader = null;
        try {
            reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(reader !=null){
                reader.close();
            }
        }
        return content;
    }
}
