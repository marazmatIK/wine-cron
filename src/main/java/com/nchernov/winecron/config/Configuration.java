package com.nchernov.winecron.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by nchernov on 06.10.2015.
 */
@XmlRootElement(name = "Configuration")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Configuration {
    private int processRate;
    private int batchSize;
    private String apiKey;
    private String endPoint;

    @XmlElement(name = "ProcessRate")
    public int getProcessRate() {
        return processRate;
    }

    public void setProcessRate(int processRate) {
        this.processRate = processRate;
    }

    @XmlElement(name = "BatchSize")
    public int getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(int batchSize) {
        this.batchSize = batchSize;
    }

    @XmlElement(name = "ApiKey")
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @XmlElement(name = "EndPoint")
    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }
}
