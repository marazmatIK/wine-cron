package com.nchernov.winecron.runner;

import com.nchernov.winecron.WineCron;
import com.nchernov.winecron.client.WineClient;
import com.nchernov.winecron.client.impl.WineClientImpl;
import com.nchernov.winecron.config.ConfigService;
import com.nchernov.winecron.config.Configuration;
import com.nchernov.winecron.mock.MockWineClient;
import com.nchernov.winecron.storage.LastSavedStateProvider;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

/**
 * Created by nchernov on 06.10.2015.
 */
public class CronRunner {

    private static final Logger log = Logger.getLogger(CronRunner.class.getName());
    private static Options options;

    private static final String COMMAND_START = "start";
    private static final String COMMAND_STOP = "stop";
    //private static final String COMMAND_EXIT = "exit";

    private static final String OPTION_CONFIG_FILE_SHORT = "f";
    private static final String OPTION_CONFIG_FILE_LONG = "file";
    private static final String OPTION_CONFIG_FILE_DESCRIPTION = "Path to the *.xml config file";

    private static final String OPTION_UPDATE_FLAG_SHORT = "u";
    private static final String OPTION_UPDATE_FLAG_LONG = "update";
    private static final String OPTION_UPDATE_FLAG_DESCRIPTION = "Update whole catalog flag. Cron will request all Wines from start and will update local database.";
    private static final String DEFAULT_CFG_PATH = "./cfg.xml";
    private static String filePath = DEFAULT_CFG_PATH;
    private static boolean updateMode = false;
    private static boolean started = false;

    /**
     * Components declaration section
     */
    private static WineCron wineCron;
    private static WineCron mockCron;
    private static WineClient wineClient;
    private static ConfigService configService = new ConfigService();

    private static final HelpFormatter formatter = new HelpFormatter();

    public static void main(String[] args) {
        CommandLineParser parser = new GnuParser();
        initOptions();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args );
            parseArgs(line);

            //line.hasOption()
        }
        catch(ParseException exp) {
            // oops, something went wrong
            log.error("Could not recognize command. Reason: " + exp.getMessage());
            formatter.printHelp("start", options);
        }

        try {
            startCron();
        } catch (Exception ex) {
            log.error("Could not start cron. Reason: " + ex.getMessage());
        }
    }

    private static void parseArgs(CommandLine line) {
        if (!line.getArgList().contains(COMMAND_START) && !line.getArgList().contains(COMMAND_STOP)) {
            log.error(String.format("Pass '%s' command to target *.jar", COMMAND_START + "|" + COMMAND_STOP));
            formatter.printHelp(COMMAND_START + "|" + COMMAND_STOP, options);
            System.exit(-1);
        }
        if (line.hasOption(OPTION_CONFIG_FILE_SHORT)) {
            filePath = line.getOptionValue(OPTION_CONFIG_FILE_SHORT);
        } else if (line.hasOption(OPTION_CONFIG_FILE_LONG)) {
            filePath = line.getOptionValue(OPTION_CONFIG_FILE_LONG);
        } else {
            log.warn(OPTION_CONFIG_FILE_DESCRIPTION + " flag was ommitted. Using default location: " + filePath);
        }

        if (line.hasOption(OPTION_UPDATE_FLAG_SHORT) || line.hasOption(OPTION_UPDATE_FLAG_LONG)) {
            updateMode = true;
        } else {
            log.warn(OPTION_UPDATE_FLAG_SHORT + " flag was ommitted. Using defalut mode: 'DOWNLOAD from last saved offset'");
        }

        if (line.getArgList().contains(COMMAND_START)) {
            log.info("Received 'start' signal. Starting cron..");
            started = true;
        }
        if (!started) {
            log.error("Cron has not been started yet. Use 'start' command before calling 'stop'.");
            System.exit(-1);
        } else if (line.getArgList().contains(COMMAND_STOP)) {
            log.info("Received 'stop' signal. Stopping cron..");
            started = false;
            System.exit(-1);
        }
    }

    private static void startCron() throws Exception {
        if (updateMode) {
            log.warn(OPTION_UPDATE_FLAG_SHORT + " flag was specified. Updating already parsed Wine items from start position (offset is again = 0)");
            LastSavedStateProvider.saveLastSavedOffset(0L);
        }
        Configuration configuration = configService.getConfig(filePath);
        wineClient = new WineClientImpl(configuration.getApiKey(), configuration.getEndPoint());
        wineCron = new WineCron(wineClient);
        wineCron.init(filePath);
    }

    private static void initOptions() {
        options = new Options();
        options.addOption(OPTION_CONFIG_FILE_SHORT, OPTION_CONFIG_FILE_LONG, true, OPTION_CONFIG_FILE_DESCRIPTION);
        options.addOption(OPTION_UPDATE_FLAG_SHORT, OPTION_UPDATE_FLAG_LONG, false, OPTION_UPDATE_FLAG_DESCRIPTION);
    }
}
