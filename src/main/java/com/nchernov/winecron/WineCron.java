package com.nchernov.winecron;

import com.nchernov.winecron.client.WineClient;
import com.nchernov.winecron.config.ConfigService;
import com.nchernov.winecron.config.Configuration;
import com.nchernov.winecron.entity.Wine;
import com.nchernov.winecron.storage.LastSavedStateProvider;
import com.nchernov.winecron.storage.WineDAO;
import com.nchernov.winecron.storage.WineDAOImpl;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by nchernov on 06.10.2015.
 */
public class WineCron implements Runnable {
    private static final Logger log = Logger.getLogger(WineCron.class.getName());
    private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private final ConfigService configService = new ConfigService();
    private WineClient wineClient;
    private volatile ScheduledFuture<?> future = null;
    private volatile int minutesRate;
    private long currentOffset = 0;
    private int batchSize;

    private Configuration configuration = new Configuration();
    private WineDAO wineDAO = new WineDAOImpl();

    public WineCron (WineClient wineClient) {
        this.wineClient = wineClient;
        configService.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object config) {
                synchronized (WineCron.this) {
                    Configuration cfg = (Configuration) config;
                    try {
                        if (cfg.getProcessRate() != minutesRate) {
                            minutesRate = cfg.getProcessRate();
                            if (future != null) {
                                future.cancel(false);
                            }
                            if (minutesRate != 0) {
                                batchSize = configuration.getBatchSize();
                                future = executor.scheduleAtFixedRate(WineCron.this, 10, minutesRate, TimeUnit.MINUTES);
                            } else {
                                future = null;
                            }
                        }
                    } catch (Throwable ex) {
                        log.error(ex);
                    }
                }
            }
        });
    }

    public void init (String configFilePath) throws Exception {
        Configuration configuration = configService.getConfig(configFilePath);
        minutesRate = configuration.getProcessRate();
        batchSize = configuration.getBatchSize();
        if (minutesRate > 0) {
            future = executor.scheduleAtFixedRate(WineCron.this, 10, minutesRate, TimeUnit.SECONDS);
        }
        if (LastSavedStateProvider.hasSavedOffset()) {
            currentOffset = LastSavedStateProvider.loadLastSavedOffset();
        } else {
            LastSavedStateProvider.saveLastSavedOffset(0L);
            currentOffset = 0;
        }
        log.info("Starting from offset: " + currentOffset);
    }

    public synchronized void destroy() throws Exception {
        if (future != null) {
            future.cancel(false);
        }
        executor.shutdownNow();
    }

    @Override
    public synchronized void run() {
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + currentOffset);
        try {
            if (Thread.currentThread().isInterrupted()) {
                LastSavedStateProvider.saveLastSavedOffset(currentOffset);
                log.error("Cron: Shutting down gracefully");
                destroy();
            }
            List<? extends Wine> wineCatalogPart = wineClient.loadItems(currentOffset, batchSize);
            for (Wine wine: wineCatalogPart) {
                log.info("]> " + wine.getParsedId());
            }
            wineDAO.save(wineCatalogPart);

            currentOffset += batchSize;
            log.info("Saving last passed offset into local database: " + currentOffset);
            LastSavedStateProvider.saveLastSavedOffset(currentOffset);
            for (Wine wine: wineCatalogPart) {
                log.info(wine);
            }
        } catch (Throwable ex) {
            log.error(String.format("Error retrieving batch from range (%s;%d)", new Long(currentOffset).toString(), batchSize), ex);
        }
    }
}
