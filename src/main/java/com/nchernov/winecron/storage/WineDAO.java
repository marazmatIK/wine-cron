package com.nchernov.winecron.storage;

import com.nchernov.winecron.entity.Wine;

import java.util.List;

/**
 * Created by nchernov on 06.10.2015.
 */
public interface WineDAO<W> {
    public List<? extends W> save(List<? extends W> wineList);
    public List<? extends W> load(List<? extends W> wineList);
}
