package com.nchernov.winecron.storage;

import com.nchernov.winecron.entity.Wine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by nchernov on 06.10.2015.
 */
@Entity
@Table(name = "wine")
@XmlRootElement(name = "Product")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class StorableWine extends Wine {
    @Column(name="title", length = 300)
    @XmlElement(name = "Name")
    private String title;

    private int year;
    @Column(name="priceMax")
    @XmlElement(name = "PriceMax")
    private float priceMax;
    @Column(name="priceMin")
    @XmlElement(name = "PriceMin")
    private float priceMin;
    @Column(name="priceRetail")
    @XmlElement(name = "PriceRetail")
    private float priceRetail;
    @Column(name="producer")
    private String producer;
    @Column(name="url")
    @XmlElement(name = "Url")
    private String url;

    @XmlElement(name = "Id")
    /** id of the Wine */
    @Id
    @Column(name="id")
    private Long parsedId;

    public Long getParsedId() {
        return parsedId;
    }

    public void setParsedId(Long parsedId) {
        this.parsedId = parsedId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        String[] tokens = title.split(" ", -1);
        for (int i = 0; i < tokens.length; i++) {
            try {
                int candidate = Integer.parseInt(tokens[i]);
                if (candidate > 99) {
                    year = candidate;
                    break;
                }
            } catch (NumberFormatException ex) {
                //ignore
            }
        }
    }

    public int getYear() {
        String[] tokens = title.split(" ", -1);
        for (int i = 0; i < tokens.length; i++) {
            try {
                int candidate = Integer.parseInt(tokens[i]);
                if (candidate > 99) {
                    year = candidate;
                    return year;
                }
            } catch (NumberFormatException ex) {
                //ignore
            }
        }
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public float getPriceMax() {
        return priceMax;
    }

    public void setPriceMax(float priceMax) {
        this.priceMax = priceMax;
    }

    public float getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(float priceMin) {
        this.priceMin = priceMin;
    }

    public float getPriceRetail() {
        return priceRetail;
    }

    public void setPriceRetail(float priceRetail) {
        this.priceRetail = priceRetail;
    }
}
