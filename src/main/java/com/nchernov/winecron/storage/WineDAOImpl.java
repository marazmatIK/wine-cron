package com.nchernov.winecron.storage;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.nchernov.winecron.entity.Wine;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by nchernov on 06.10.2015.
 */
public class WineDAOImpl implements WineDAO<Wine> {

    private SessionFactory sessionFactory;

    public WineDAOImpl () {
        sessionFactory = HibernateUtil.getSessionFactory();
    }

    @Override
    public List<? extends Wine> save(List<? extends Wine> wineList) {
        return transaction(Action.SAVE, wineList);
    }

    @Override
    public List<? extends Wine> load(List<? extends Wine> wineList) {
        return transaction(Action.DELETE, wineList);
    }

    private interface Action {
        List<? extends Wine> perform(Session session, List<? extends Wine> wineList);

        public Action DELETE = new Action() {
            @Override
            public List<? extends Wine> perform(Session session, List<? extends Wine> wineList) {

                for(Wine storableWine: wineList) {
                    session.delete(storableWine);
                }
                return wineList;
            }
        };
        public Action SAVE = new Action() {
            @Override
            public List<? extends Wine> perform(Session session, List<? extends Wine> wineList) {
                for(Wine storableWine: wineList) {
                    try {
                        session.saveOrUpdate(storableWine);
                    } catch (Exception ex) {
                        session.delete(storableWine);
                        session.saveOrUpdate(storableWine);
                    }
                }
                return wineList;
            }
        };

    }

    /*@Override
    public List<StorableWine> save(List<StorableWine> wineList) {
        return null;
    }

    @Override
    public List<StorableWine> load(List<StorableWine> wineList) {
        return null;
    }*/

    public Collection<StorableWine> find(String query) {
        Session session = null;
        Collection<StorableWine> wineList = Collections.emptyList();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query queryObj = session.createQuery(query);
            List list = queryObj.list();
            session.getTransaction().commit();
            wineList = Collections2.transform(list, new Function() {
                @Override
                public StorableWine apply(@Nullable Object o) {
                    return (StorableWine) o;
                }
            });
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            return wineList;
        }
    }

    private List<? extends Wine> transaction(Action action, List<? extends Wine> wineList) {
        Session session = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            synchronized (sessionFactory) {
                session.beginTransaction();
                List storableWineList = action.perform(session, wineList);
                session.getTransaction().commit();
                return storableWineList;
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
