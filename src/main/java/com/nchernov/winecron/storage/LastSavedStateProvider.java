package com.nchernov.winecron.storage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Storage for last saved offset of parsed Wine catalog objects.
 */
public class LastSavedStateProvider {
    private static final String LAST_SAVED_TIMESTAMP_FILE = ".offset";

    public static boolean hasSavedOffset() throws IOException {
        File file = new File(LAST_SAVED_TIMESTAMP_FILE);
        return file.exists();
    }

    public static long loadLastSavedOffset() throws IOException {
        File file = new File(LAST_SAVED_TIMESTAMP_FILE);
        if (file.exists()) {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = bufferedReader.readLine();
            return Long.parseLong(line);
        } else {
            return 0;
        }
    }

    public static void saveLastSavedOffset(long lastSavedOffset) throws IOException {
        File file = new File(LAST_SAVED_TIMESTAMP_FILE);
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(new Long(lastSavedOffset).toString());
        fileWriter.close();
    }
}
