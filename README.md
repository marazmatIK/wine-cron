# Requirements
JDK 1.6 or later; Apache Maven 3; cmd tool (linux or win terminal, for instance FAR Manager for Windows OS); Internet Connection for the dependencies and Wine catalog data;
# Build process

1 Go to $PROJECT directory
2 Issue command 

    mvn clean package

or

    sudo mvn package

on Linux systems

There will be compilation, copying resoruces and libs processes executed. The target *.jar file will be placed in $PROJECT/target/bin folder along with lib folder of dependencies

# Execution process
1 Go to $PROJECT/target/bin folder

2 Place your custom cfg.xml file if you prefer not to use -f (--file) arg for target config file.

3 Issue command 

    java -jar wine-cron-0.0.1.jar start 

to start(continue) cron downloading process from latest memorized offset position or 

    java -jar wine-cron-0.0.1.jar -u start 

to start downloading process (e.g. updating process) from very start offset (0 in terms of Wine APIs parameters)

# Command line format
Following commands are supported now: start|stop
    
    start command can be executed with additional (optional) arguments:
    -f,--file <arg>   Path to the *.xml config file
    -u,--update       Update whole catalog flag. Cron will request all Wines
                   from start and will update local database.
                   
# Configuration file for Cron
Configuration file sample is presented bellow:

    <Configuration>
        <!-- Every request will try to obtain $BatchSize new items from service -->
        <BatchSize>2</BatchSize>
        <!-- Place your own API KEY HERE -->
        <ApiKey>9da1a18698da0ff182f08b04d2c276e4</ApiKey>
        <!-- Time unit: minute. 0 means - never -->
        <ProcessRate>1</ProcessRate>
    </Configuration>
    
You can configure endpoint of service, process rate (in minutes, 0 means never), ApiKey for endpoint and Batch size of updates

# Configuring database
Before building process one can customize storage location. Its HSQL embedded file database configured by hibernate.cfg.xml resoruce file.
Find the followintg XML element within a file:

    <property name="hibernate.connection.url">
        jdbc:hsqldb:file:d:/work/WineCron/storage/derby;sql.ignore_case=true
    </property>

Edit it for your custom location, for instance: 

    <property name="hibernate.connection.url">
        jdbc:hsqldb:file:/home/WineCron/storage/derby;sql.ignore_case=true
    </property>
    
Save new configuration then rebuild the project.

# Using Wine mock service
There is Maven project present in $PROJECT/mock folder. It mocks real wine.com API services. You can try it via http://localhost:9090/rest/catalog endpoint. For instance, try query: http://localhost:9090/rest/catalog?offset=2&size=3
To run such a mock service one should perform following steps:
1. Go to $PROJECT/mock folder
2. Execute 

    mvn tomcat7:run 
    
3. Embedded tomcat will be started up in mock directory under nested /target dir. Enjoy!